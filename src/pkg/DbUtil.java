package pkg;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;

public enum DbUtil {
	INST;

	private static Logger logger_ = Logger.getLogger(DbUtil.class);

	private final static String MYSQL_VALID_QUERY = "select 1";
	private final static int MAX_IDLE = 4;
	private final static int EVICTION_INTERVAL = 60000; // 60 seconds

	private static BasicDataSource data_src = new BasicDataSource();
	private String schema = "";

	public void init() throws Exception {
		Properties prop = StaticInfoMain.getProperties();
		data_src.setDriverClassName("com.mysql.jdbc.Driver");
		data_src.setUrl(prop.getProperty("db_conn"));
		data_src.setUsername(prop.getProperty("db_user"));
		data_src.setPassword(prop.getProperty("db_pswd"));
		data_src.setValidationQuery(MYSQL_VALID_QUERY);
		data_src.setTestOnBorrow(true);
		data_src.setTestWhileIdle(true);
		data_src.setTimeBetweenEvictionRunsMillis(EVICTION_INTERVAL);
		data_src.setNumTestsPerEvictionRun(MAX_IDLE);
		data_src.setMaxIdle(MAX_IDLE);

		String tokens[] = prop.getProperty("db_conn").split("/");
		schema = tokens[tokens.length - 1];
		// System.out.println(schema);

		Connection conn = data_src.getConnection();
		conn.close();
	}

	public void free() {
		try {
			if (null != data_src)
				data_src.close();
		} catch (Exception e) {
			logger_.error("Exception when closing data_src: ", e);
		}
	}

	public String getSchemaName() {
		return schema;
	}

	// public void exec(String sql) {
	// try (Connection conn = data_src.getConnection(); Statement stmt =
	// conn.createStatement();) {
	// stmt.executeUpdate(sql);
	// } catch (Exception ex) {
	// logger_.error("Fail to run sql=" + sql, ex);
	// }
	// }

	public Connection getConn() throws SQLException {
		return data_src.getConnection();
	}
}
