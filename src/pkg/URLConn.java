package pkg;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.apache.log4j.Logger;

public class URLConn {
	private static Logger logger_ = Logger.getLogger(URLConn.class);

	public static List<String> load_url(String url, int time_out) throws MalformedURLException, IOException {
		URLConnection conn = new URL(url).openConnection();
		conn.setRequestProperty("Cookie", "userId=Alice");
		conn.setConnectTimeout(time_out);
		conn.setReadTimeout(time_out);
		conn.connect();

		InputStream orig_stream = null;
		BufferedInputStream buff_stream = null;
		GZIPInputStream gzip_stream = null;
		try {
			orig_stream = conn.getInputStream();
			InputStream stream = orig_stream;
			if (!orig_stream.markSupported()) {
				buff_stream = new BufferedInputStream(orig_stream);
				stream = buff_stream;
			}
			stream.mark(2);
			int magic = 0;
			try {
				magic = stream.read() & 0xff | ((stream.read() << 8) & 0xff00);
				stream.reset();
			} catch (IOException e) {
				logger_.error("IOException: ", e);
			}
			if (magic == GZIPInputStream.GZIP_MAGIC) {
				gzip_stream = new GZIPInputStream(stream);
				stream = gzip_stream;
			}
			try (InputStreamReader ins_reader = new InputStreamReader(stream);
					BufferedReader buff_reader = new BufferedReader(ins_reader);) {
				String line;
				List<String> line_arr = new ArrayList<String>();
				while ((line = buff_reader.readLine()) != null) {
					line_arr.add(line);
				}
				return line_arr;
			}
		} finally {
			if (orig_stream != null)
				orig_stream.close();
			if (buff_stream != null)
				buff_stream.close();
			if (gzip_stream != null)
				gzip_stream.close();
		}
	}
}
