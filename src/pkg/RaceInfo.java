package pkg;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

public class RaceInfo {

	private static Logger logger_ = Logger.getLogger(RaceInfo.class);
	private String race_date;
	private String venue;
	private int total_races;

	RaceInfo(String race_date, String venue, int total_races) {
		this.race_date = race_date;
		this.venue = venue;
		this.total_races = total_races;
	}

	String get_race_date() {
		return this.race_date;
	}

	String get_venue() {
		return this.venue;
	}

	int get_total_races() {
		return this.total_races;
	}

	public static RaceInfo get_next_race_info(int load_wait_time) {
		logger_.info("Get race info for next race");
		boolean loading = false;
		String date = null;
		String venue = null;
		int total_races = 0;
		String basic_url = "http://racing.hkjc.com/racing/Info/meeting/RaceCard/english/Local/";
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.MINUTE, +2);
			do {
				Date curr = Calendar.getInstance().getTime();
				if (curr.after(calendar.getTime())) {
					break;
				}
				loading = false;
				List<String> line_arr = URLConn.load_url(basic_url, load_wait_time);
				for (String inputLine : line_arr) {
					if (inputLine.contains("Loading.gif")) {
						logger_.info("the page is still loading");
						loading = true;
						break;
					}
					if (inputLine.contains("/racing/Info/StaticFile/Images/Racing/racecard_rt")) {
						inputLine = inputLine.substring(inputLine.lastIndexOf("Local") + 6);
						date = inputLine.substring(0, 8);
						inputLine = inputLine.substring(inputLine.indexOf("/") + 1);
						venue = inputLine.substring(0, inputLine.indexOf("/"));
						total_races = Integer
								.parseInt(inputLine.substring(inputLine.indexOf("/") + 1, inputLine.indexOf("\"")));
						break;
					}
				}
			} while (loading);
		} catch (Exception e) {
			logger_.error("Exception: ", e);
		}
		logger_.info("Next race date: " + date);
		return new RaceInfo(date, venue, total_races);
	}
}
