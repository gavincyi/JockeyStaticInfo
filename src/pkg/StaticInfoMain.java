package pkg;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class StaticInfoMain {

	private static final String LOG4J_CONFIG_FILE = "log4j.properties";
	private static Logger logger_ = Logger.getLogger(StaticInfoMain.class);

	private static Properties properties_ = new Properties();

	public static Properties getProperties() {
		return properties_;
	}

	private static Integer check_integer(String inputLine) {
		Integer temp = null;
		if (inputLine.matches("\\d+")) {
			temp = Integer.parseInt(inputLine);
		}
		return temp;
	}

	public static void main(String[] args) {

		try {
			PropertyConfigurator.configure(LOG4J_CONFIG_FILE);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}

		final int load_wait_time = 60 * 1000;

		try {
			properties_.load(new FileInputStream("config.properties"));
			DbUtil.INST.init();
			try (Connection conn = DbUtil.INST.getConn(); Statement stmt = conn.createStatement();) {
				String query = "select table_name from information_schema.tables where TABLE_SCHEMA = '"
						+ DbUtil.INST.getSchemaName() + "' and "
						+ "TABLE_NAME like 'hkjc\\_race\\_info\\_________' order by table_name DESC limit 1";

				String table = null;
				try (ResultSet rs = stmt.executeQuery(query)) {
					if (rs.next()) {
						table = rs.getString(1);
					}
				}
				logger_.info("last local race table: " + table);
				String pre_date = table.substring(table.lastIndexOf("_") + 1);

				Calendar cal = Calendar.getInstance();
				// cal.add(Calendar.DATE, -1);
				String ytd = new SimpleDateFormat("yyyyMMdd").format(cal.getTime());
				RaceInfo race_info = RaceInfo.get_next_race_info(load_wait_time);
				String race_date = race_info.get_race_date();

				if (race_date != null && Integer.parseInt(ytd) > Integer.parseInt(pre_date)) {
					DatabaseMetaData dbm = conn.getMetaData();
					try (ResultSet tables = dbm.getTables(null, null, "hkjc_draw_stats_" + race_date, null)) {
						if (tables.next()) {
						} else {
							update_draw_stats(race_date, load_wait_time, stmt);
							update_all_draw_stats(race_date, load_wait_time, stmt);
							update_new_horse_info(race_date, pre_date, load_wait_time, stmt, conn);
							List<String> horse_code_list = get_all_horse_code(load_wait_time);
							update_all_horse_form_rec(race_date, pre_date, load_wait_time, stmt, conn, horse_code_list);
							update_horse_performance(race_date, pre_date, load_wait_time, stmt, horse_code_list);
							update_same_sire_performance(race_date, pre_date, load_wait_time, stmt, horse_code_list);
							update_ranking(race_date, load_wait_time, stmt);
							update_jockey_trainer_info(race_date, pre_date, load_wait_time, stmt);
							update_horse_rating(race_date, load_wait_time, stmt);

						}
					}
				}
				logger_.info("main ended");
			}
		} catch (SQLException se) {
			logger_.info("SQLException: ", se);
		} catch (Exception e) {
			logger_.error("Exception:", e);
		} finally {
			DbUtil.INST.free();
			logger_.info("stopped");
		}
	}

	public static void update_draw_stats(String date, int load_wait_time, Statement stmt) {

		logger_.info("Start getting draw statistics for date " + date);
		try {
			String table_name = "hkjc_draw_stats_" + date;
			String sql = "CREATE TABLE if not exists " + table_name
					+ " (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, race INT, dist INT, venue varchar(5), track varchar(10), "
					+ "course varchar(10), draw INT, runners INT, win INT, 2nd INT, 3rd INT, 4th INT, W_pct INT, Q_pct INT, "
					+ "P_pct INT, F_pct INT, fav_win_pct INT, fav_pla_pct INT, fav_first4_pct INT)";
			stmt.executeUpdate(sql);

			boolean loading = false;
			do {
				loading = false;
				List<String> line_arr = URLConn
						.load_url("http://racing.hkjc.com/racing/info/meeting/Draw/English/Local", load_wait_time);
				String venue = null;
				int race = 0;
				int length = 0;
				String track = null;
				String course = null;
				int fav_win = 0;
				int fav_pla = 0;
				int fav_first4 = 0;
				boolean begin_flag = false;
				List<List<Integer>> records = new ArrayList<List<Integer>>();
				List<Integer> rec = new ArrayList<Integer>();

				for (String inputLine : line_arr) {
					if (inputLine.contains("Loading.gif")) {
						logger_.info("the page is still loading");
						loading = true;
						break;
					}

					if (inputLine.contains("rowDiv30")) {
						begin_flag = false;
						records = new ArrayList<List<Integer>>();
						rec = new ArrayList<Integer>();
					} else if (inputLine.contains("blueBtn")) {
						venue = inputLine.substring(inputLine.indexOf("-") + 2);
						venue = venue.substring(0, 1) + venue.charAt(venue.indexOf(" ") + 1);
					} else if (inputLine.contains("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;")) {
						begin_flag = true;
						String arr[] = inputLine.split("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

						race = Integer.parseInt(arr[0].substring(arr[0].indexOf("R") + 5));
						length = Integer.parseInt(arr[1].substring(0, arr[1].length() - 1));
						track = arr[2];
						if (arr.length > 3)
							course = arr[3].substring(1, arr[3].lastIndexOf("\""));
						else
							course = null;
					} else if (begin_flag == true && inputLine.contains("td class=\"tdBgYellow2 bold\"")) {
						rec.add(Integer
								.parseInt(inputLine.substring(inputLine.indexOf(">") + 1, inputLine.lastIndexOf("<"))));
					} else if (begin_flag == true && inputLine.contains("<td>") && !inputLine.contains("trBgWhite")) {
						rec.add(Integer
								.parseInt(inputLine.substring(inputLine.indexOf(">") + 1, inputLine.lastIndexOf("<"))));
					} else if (begin_flag == true && inputLine.contains("td class=\"tdAlignL\"")) {
						records.add(rec);
						rec = new ArrayList<Integer>();
					} else if (begin_flag == true && inputLine.contains("&nbsp;")) {
						String arr[] = inputLine.split("&nbsp;&nbsp;");
						fav_win = Integer
								.parseInt(arr[1].substring(arr[1].lastIndexOf("&nbsp;") + 6, arr[1].length() - 1));
						fav_pla = Integer
								.parseInt(arr[2].substring(arr[2].lastIndexOf("&nbsp;") + 6, arr[2].length() - 1));
						fav_first4 = Integer
								.parseInt(arr[3].substring(arr[3].lastIndexOf("&nbsp;") + 6, arr[3].length() - 1));

						sql = "insert into " + table_name + " values ";
						for (int i = 0; i < records.size(); i++) {
							sql += " (NULL, " + race + "," + length + ",\"" + venue + "\",\"" + track + "\",\"" + course
									+ "\",";
							for (int j = 0; j < records.get(i).size(); j++) {
								sql += records.get(i).get(j) + ",";
							}

							sql += fav_win + "," + fav_pla + "," + fav_first4 + ")";

							if (i < records.size() - 1)
								sql += ",";
							else
								sql += ";";
						}

						stmt.executeUpdate(sql);
						begin_flag = false;
					}
				}
			} while (loading == true);

		} catch (SQLException se) {
			logger_.error("SQLException:", se);
		} catch (Exception e) {
			logger_.error("Exception:", e);
		}
		logger_.info("Finish getting draw statistics for date " + date);
	}

	public static void update_all_draw_stats(String date, int load_wait_time, Statement stmt) {

		logger_.info("Start getting all draw statistics for date " + date);
		try {
			String table_name = "hkjc_all_draw_stats_" + date;
			String sql = "CREATE TABLE if not exists " + table_name
					+ " (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, season varchar(50), venue varchar(5), dist INT, track varchar(10), "
					+ "going varchar(50), course varchar(10), draw INT, runners INT, win INT, 2nd INT, 3rd INT, 4th INT, W_pct INT, "
					+ "Q_pct INT, P_pct INT, F_pct INT, fav_win_pct INT, fav_pla_pct INT, fav_first4_pct INT)";
			stmt.executeUpdate(sql);

			boolean loading = false;
			String basic_url = "http://www.hkjc.com/english/racing/draw_advsearch.asp?";
			ArrayList<String> year_opts = new ArrayList<String>();
			ArrayList<String> track_opts = new ArrayList<String>();
			ArrayList<ArrayList<ArrayList<String>>> course_opts = new ArrayList<ArrayList<ArrayList<String>>>();
			ArrayList<ArrayList<String>> distance_opts = new ArrayList<ArrayList<String>>();
			ArrayList<ArrayList<String>> going_opts = new ArrayList<ArrayList<String>>();
			String url_addr;

			do {
				loading = false;

				List<String> line_arr = URLConn.load_url(basic_url, load_wait_time);
				boolean start = false;
				boolean track_occurred = false;
				boolean course_occurred = false;
				String pre_opt = null;
				ArrayList<ArrayList<String>> course_list = new ArrayList<ArrayList<String>>();
				ArrayList<String> temp_course_list = new ArrayList<String>();
				ArrayList<String> temp_dist_list = new ArrayList<String>();
				ArrayList<String> temp_going_list = new ArrayList<String>();

				for (String inputLine : line_arr) {

					if (inputLine.contains("Loading.gif")) {
						logger_.info("the page is still loading");
						loading = true;
						break;
					}

					if (pre_opt != null && !inputLine.contains(pre_opt)) {
						if (temp_course_list.size() > 0) {
							course_list.add(temp_course_list);
							temp_course_list = new ArrayList<String>();
						} else if (temp_dist_list.size() > 0) {
							distance_opts.add(temp_dist_list);
							temp_dist_list = new ArrayList<String>();
						} else if (temp_going_list.size() > 0) {
							going_opts.add(temp_going_list);
							temp_going_list = new ArrayList<String>();
						}

					}

					if (inputLine.contains("function changeValue")) {
						start = true;
					} else if (start == true) {

						if (inputLine.contains("else")) {
							course_opts.add(course_list);
							course_list = new ArrayList<ArrayList<String>>();
						}

						if (inputLine.contains("Distance")) {
							pre_opt = "Distance";
							inputLine = inputLine.substring(inputLine.indexOf("'") + 1, inputLine.lastIndexOf("'"));
							temp_dist_list.add(inputLine);
						} else if (inputLine.contains("tyear == ")) {
							if (inputLine.contains("'"))
								inputLine = inputLine.substring(inputLine.indexOf("'") + 1, inputLine.lastIndexOf("'"));
							else
								inputLine = inputLine.substring(inputLine.indexOf("==") + 3, inputLine.indexOf(")"));
							if (Integer.parseInt(inputLine) > 2012) {
								course_occurred = true;
							}
						} else if (course_occurred == true && inputLine.contains("Course")) {
							pre_opt = "Course";
							inputLine = inputLine.substring(inputLine.indexOf("'") + 1, inputLine.lastIndexOf("'"));
							temp_course_list.add(inputLine);
						} else if (course_occurred == true && inputLine.contains("}")) {
							course_occurred = false;
						} else if (inputLine.contains("} else {")) {
							course_occurred = true;
						} else if (inputLine.contains("Going")) {
							pre_opt = "Going";
							inputLine = inputLine.substring(inputLine.indexOf("'") + 1, inputLine.lastIndexOf("'"));
							temp_going_list.add(inputLine);
						} else if (inputLine.contains(" = ''")) {
							start = false;
						}
					}

					if (inputLine.contains("Season ")) {
						String[] arr = inputLine.split("</Option>");
						for (int i = 0; i < arr.length - 1; i++) {
							String value = arr[i].substring(arr[i].indexOf("\"") + 1, arr[i].lastIndexOf("\""));
							year_opts.add(value);
						}
					} else if (inputLine.contains("\"Racecourse\"")) {
						track_occurred = true;
					} else if (track_occurred == true && inputLine.contains("Option")) {
						String value = inputLine.substring(inputLine.indexOf("\"") + 1, inputLine.lastIndexOf("\""));
						track_opts.add(value);
					} else if (track_occurred == true && inputLine.contains("/select")) {
						track_occurred = false;
					}

				}

			} while (loading == true);
			Collections.sort(track_opts);
			Collections.reverse(track_opts);

			for (int track_idx = 0; track_idx < track_opts.size(); track_idx++) {
				for (int dist_idx = 0; dist_idx < distance_opts.get(track_idx).size(); dist_idx++) {
					for (int going_idx = 0; going_idx < going_opts.get(track_idx).size(); going_idx++) {
						for (int year_idx = 0; year_idx < year_opts.size(); year_idx++) {
							int course_opt_size = 0;
							if (track_idx != 1)
								course_opt_size = course_opts.get(track_idx).get(year_idx).size();
							else
								course_opt_size = 1;

							for (int course_idx = 0; course_idx < course_opt_size; course_idx++) {

								if (track_idx != 1)
									url_addr = basic_url + "Year=" + year_opts.get(year_idx) + "&Racecourse="
											+ track_opts.get(track_idx) + "&Course="
											+ course_opts.get(track_idx).get(year_idx).get(course_idx) + "&Distance="
											+ distance_opts.get(track_idx).get(dist_idx) + "&Going="
											+ going_opts.get(track_idx).get(going_idx);
								else
									url_addr = basic_url + "Year=" + year_opts.get(year_idx) + "&Racecourse="
											+ track_opts.get(track_idx) + "&Course="
											+ course_opts.get(track_idx).get(0).get(course_idx) + "&Distance="
											+ distance_opts.get(track_idx).get(dist_idx) + "&Going="
											+ going_opts.get(track_idx).get(going_idx);

								do {
									loading = false;
									logger_.info("Getting the draw statistics in the url: " + url_addr);

									List<String> line_arr = URLConn.load_url(url_addr, load_wait_time);
									boolean header_begin = false;
									boolean fav_begin = false;
									boolean begin_flag = false;
									int cnt = 0;
									int tr_cnt = 0;
									List<List<Integer>> records = new ArrayList<List<Integer>>();
									List<Integer> rec = new ArrayList<Integer>();
									int fav_win = 0;
									int fav_pla = 0;
									int fav_first4 = 0;
									String season = "";
									String venue = "";
									String track = "";
									int dist = 0;
									String course = "";
									String going = "";

									for (String inputLine : line_arr) {
										if (inputLine.contains("Loading.gif")) {
											logger_.info("the page is still loading");
											loading = true;
											break;
										}

										if (inputLine.contains("<td bgcolor=\"#005297\" class=table_eng_text><font")) {
											header_begin = true;
											cnt = 0;
										} else if (header_begin == true && !inputLine.contains("</b>")) {
											++cnt;
											inputLine = inputLine.trim();
											switch (cnt) {
											case 1:
												season = inputLine.substring(0, inputLine.indexOf("&"));
												break;
											case 2:
												venue = inputLine.substring(0, 1)
														+ inputLine.charAt(inputLine.indexOf(" ") + 1);
												break;
											case 3:
												dist = Integer.parseInt(inputLine.substring(
														inputLine.lastIndexOf(";") + 1, inputLine.indexOf("m")));
												break;
											case 4:
												track = inputLine.replace("&nbsp;", "");
												break;
											case 5:
												going = inputLine;
												break;
											case 7:
												course = inputLine;
												if (course.contains("\""))
													course = course.substring(1, course.lastIndexOf("\""));
												break;
											default:
												break;
											}

										} else if (header_begin == true && inputLine.contains("</b>")) {
											header_begin = false;
										} else if (inputLine.contains("<td bgcolor=\"#005297\">")) {
											begin_flag = true;
										} else if (begin_flag == true && inputLine.contains("<tr>")) {
											++tr_cnt;
										} else if (tr_cnt > 1 && inputLine.contains("center")
												&& !inputLine.contains("EBEBC4")) {
											rec.add(Integer.parseInt(inputLine.substring(
													inputLine.lastIndexOf("\">") + 2, inputLine.indexOf("</"))));
										} else if (tr_cnt > 1 && inputLine.contains("</tr>")) {
											records.add(rec);
											rec = new ArrayList<Integer>();
										} else if (tr_cnt > 1 && inputLine.contains("td bgcolor=\"#FFFFFF\"")) {
											fav_begin = true;
										} else if (fav_begin == true && inputLine.contains("table_eng_text")) {
											fav_win = Integer.parseInt(inputLine.substring(inputLine.indexOf("\">") + 2,
													inputLine.indexOf("%")));
										} else if (fav_begin == true && inputLine.contains("Placed")) {
											fav_pla = Integer.parseInt(inputLine
													.substring(inputLine.indexOf("</b>") + 4, inputLine.indexOf("%")));
										} else if (fav_begin == true && inputLine.contains("First")) {
											fav_first4 = Integer.parseInt(inputLine
													.substring(inputLine.indexOf("</b>") + 4, inputLine.indexOf("%")));
										} else if (fav_begin == true && inputLine.contains("296")) {

											sql = "insert into " + table_name + " values";
											for (int i = 0; i < records.size(); i++) {
												sql += " (NULL, \"" + season + "\",\"" + venue + "\"," + dist + ",\""
														+ track + "\",\"" + going + "\",\"" + course + "\",";
												for (int j = 0; j < records.get(i).size(); j++) {
													sql += records.get(i).get(j) + ",";
												}

												sql += fav_win + "," + fav_pla + "," + fav_first4 + ")";

												if (i < records.size() - 1)
													sql += ",";
												else
													sql += ";";
											}

											stmt.executeUpdate(sql);
											fav_begin = false;
											tr_cnt = 0;
											begin_flag = false;
										}

									}
								} while (loading == true);
							}
						}
					}
				}
			}

		} catch (SQLException se) {
			logger_.error("SQLException:", se);
		} catch (Exception e) {
			logger_.error("Exception:", e);
		}
		logger_.info("Finish getting all draw statistics for date " + date);
	}

	public static void update_horse_rating(String date, int load_wait_time, Statement stmt) {
		logger_.info("Start updating horse rating");
		try {
			String table_name = "hkjc_horse_rating_" + date;
			String sql = "CREATE TABLE if not exists " + table_name
					+ " (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, name varchar(30), brand_no varchar(5), rating SMALLINT, variation SMALLINT)";
			stmt.executeUpdate(sql);
			boolean loading = false;

			Date curr_time = new Date();
			String today = new SimpleDateFormat("yyyyMMdd").format(curr_time);
			Date dt = new SimpleDateFormat("yyyyMMdd HH:mm").parse(today + " 15:01");
			if (dt.after(curr_time)) {
				Thread.sleep(dt.getTime() - curr_time.getTime());
			}

			do {
				loading = false;

				List<String> line_arr = URLConn.load_url(
						"http://www.hkjc.com/english/racing/mcs01_xml_horserating.asp?type=CLAS", load_wait_time);
				for (String inputLine : line_arr) {
					if (inputLine.contains("Loading.gif")) {
						logger_.info("the page is still loading");
						loading = true;
						break;
					}

					if (inputLine.contains("<TABLE")) {
						inputLine = inputLine.substring(inputLine.lastIndexOf("<u>Change</u>") + 13,
								inputLine.indexOf("</TABLE>"));
						String input_arr[] = inputLine.split("&nbsp;");
						sql = "insert into " + table_name + " values ";
						for (int i = 0; i < input_arr.length; i++) {
							if (input_arr[i].contains("font")) {
								String info[] = input_arr[i].split("</font>");
								String name = info[0].substring(info[0].lastIndexOf(">") + 1);
								String brand_no = info[1].substring(info[1].lastIndexOf(">") + 1);
								int rating = Integer.parseInt(info[2].substring(info[2].lastIndexOf(">") + 1));
								int change = 0;
								if (info.length == 5 && !info[3].contains("*"))
									change = Integer.parseInt(info[3].substring(info[3].lastIndexOf(">") + 1));
								sql += "(NULL, \"" + name + "\",\"" + brand_no + "\"," + rating + "," + change + ")";
								sql += ",";
							}
						}

						sql = sql.substring(0, sql.length() - 1) + ";";
						stmt.executeUpdate(sql);
						break;
					}
				}
			} while (loading == true);

		} catch (SQLException se) {
			logger_.error("SQLException:", se);
		} catch (Exception e) {
			logger_.error("Exception:", e);
		}
		logger_.info("Finish updating horse rating");
	}

	public static void update_ranking(String date, int load_wait_time, Statement stmt) {

		String job[] = { "jockey", "trainer" };
		boolean loading = false;
		try {
			for (int k = 0; k < job.length; k++) {

				logger_.info("Start updating " + job[k] + "s' ranking");
				String basic_url = "http://racing.hkjc.com/racing/Info/" + job[k] + "/Ranking/English/Current/Numbers/";
				int option_idx = 1;
				int total_option = 4;
				String option[] = { "all", "st turf", "st awt", "hv turf" };

				String table_name = "hkjc_" + job[k] + "s_ranking_" + date;
				String sql = "CREATE TABLE if not exists " + table_name
						+ "(id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, " + job[k]
						+ " varchar(20), win INT, 2nd INT, 3rd INT, 4th INT, "
						+ "5th INT, total_rides INT, stakes_won INT, venue varchar(10), in_service varchar(1))";
				stmt.executeUpdate(sql);

				String url_addr;

				do {

					do {
						logger_.info("Start fetching " + job[k] + " ranking for option: " + option_idx);
						loading = false;
						if (option_idx == 1)
							url_addr = basic_url + "all/all";
						else
							url_addr = basic_url + option[option_idx - 1].replace(" ", "/");
						logger_.info(url_addr);
						List<String> line_arr = URLConn.load_url(url_addr, load_wait_time);
						for (String inputLine : line_arr) {

							if (inputLine.contains("Loading.gif")) {
								logger_.info("the page is still loading");
								loading = true;
								break;
							}

							if (inputLine.contains(
									job[k].substring(0, 1).toUpperCase() + job[k].substring(1) + "s' Ranking")) {
								inputLine = inputLine.substring(inputLine.indexOf("<a href=\"h"),
										inputLine.indexOf("</table>"));
								String in_service = "Y";
								String rec_arr[] = inputLine.split("</tr>");
								sql = "insert into " + table_name + " values ";

								for (int i = 0; i < rec_arr.length; i++) {
									if (rec_arr[i].contains("<a")) {
										rec_arr[i] = rec_arr[i].replaceAll("</a>", "");
										String arr[] = rec_arr[i].split("</td>");
										sql += "(NULL, \"";
										for (int j = 0; j < arr.length; j++) {
											arr[j] = arr[j].substring(arr[j].lastIndexOf(">") + 1);
											arr[j] = arr[j].replace("$", "");
											arr[j] = arr[j].replaceAll(",", "");
											sql += arr[j];
											if (j == 0)
												sql += "\",";
											else
												sql += ",";
										}

										if (option_idx > 1)
											sql += "null,";
										sql += "\"" + option[option_idx - 1] + "\",\"" + in_service + "\")";
										sql += ",";
									} else {
										in_service = "N";
									}
								}

								sql = sql.substring(0, sql.length() - 1) + ";";
								stmt.executeUpdate(sql);

							}

						}
					} while (loading == true);

					option_idx++;
					logger_.info("Finished");
				} while (option_idx <= total_option);

				logger_.info("Finish updating " + job[k] + "s' ranking");
			}

		} catch (SQLException se) {
			logger_.error("SQLException:", se);
		} catch (Exception e) {
			logger_.error("Exception:", e);
		}

	}

	public static void update_jockey_trainer_info(String date, String pre_date, int load_wait_time, Statement stmt) {

		logger_.info("Start updating jockey trainer info");

		try {
			String table_name = "detailed_jockey_trainer_info_" + date;
			String pre_table = "detailed_jockey_trainer_info_" + pre_date;

			String sql = "CREATE TABLE if not exists " + table_name + " like " + pre_table;
			stmt.executeUpdate(sql);
			sql = "insert into " + table_name + " select * from " + pre_table;
			stmt.executeUpdate(sql);

			List<String> jockey_url = new ArrayList<String>();
			List<String> in_service = new ArrayList<String>();
			boolean loading = false;

			do {
				loading = false;
				List<String> line_arr = URLConn.load_url("http://racing.hkjc.com/racing/info/Jockey/Ranking/English",
						load_wait_time);
				for (String inputLine : line_arr) {

					if (inputLine.contains("Loading.gif")) {
						logger_.info("the page is still loading");
						loading = true;
						break;
					}

					if (inputLine.contains("Jockeys' Ranking")) {
						inputLine = inputLine.substring(inputLine.indexOf("<tr class=\"trBgWhite number\">"));
						inputLine = StringEscapeUtils.unescapeHtml4(inputLine);
						String temp_input = inputLine.substring(0, inputLine.indexOf("<tr class=\"trBgWhite\">"));
						String arr[] = temp_input.split("</tr>");
						for (int i = 0; i < arr.length; i++) {
							arr[i] = arr[i].substring(arr[i].indexOf("<a") + 9);
							jockey_url.add(arr[i].substring(0, arr[i].indexOf("\"")));
							in_service.add("Y");
						}

						inputLine = inputLine.substring(inputLine.indexOf("Others") + 16);

						arr = inputLine.split("</tr>");
						for (int i = 0; i < arr.length - 1; i++) {
							arr[i] = arr[i].substring(arr[i].indexOf("<a") + 9);
							jockey_url.add(arr[i].substring(0, arr[i].indexOf("\"")));
							in_service.add("N");
						}

					}
				}
			} while (loading == true);

			for (int i = 0; i < jockey_url.size(); i++) {

				do {
					loading = false;
					String race_date = null;
					String venue = null;
					int points = 0;
					int win = 0;
					int snd = 0;
					int trd = 0;

					String rec_cols[] = new String[15];
					int total_horse = 0;
					String jockey = null;
					boolean has_pts = false;

					List<String> line_arr = URLConn.load_url(jockey_url.get(i), load_wait_time);
					for (String inputLine : line_arr) {

						if (inputLine.contains("Loading.gif")) {
							logger_.info("the page is still loading");
							loading = true;
							break;
						}

						if (inputLine.contains(" Riding Records")) {
							jockey = inputLine.substring(inputLine.indexOf(" - ") + 3, inputLine.indexOf("</b>"));
							if (in_service.get(i).equals("N")) {
								String query = "select distinct in_service from `" + table_name + "` where jockey = \""
										+ jockey + "\";";
								String service_state = "NK";
								try (ResultSet rs = stmt.executeQuery(query)) {
									if (rs.next()) {
										service_state = rs.getString(1);
									}
								}
								if (service_state.equals("Y")) {
									logger_.info("change the service state of jockey " + jockey + "to N");
									query = "UPDATE `" + table_name + "` set in_service = \"N\" where jockey = \""
											+ jockey + "\";";
									stmt.executeUpdate(query);
								}
							}
							logger_.info(jockey);
						}

						if (inputLine
								.contains(new SimpleDateFormat("dd/MM/yyyy")
										.format(new SimpleDateFormat("yyyyMMdd").parse(pre_date)))
								&& inputLine.contains("td")) {
							String rows[] = inputLine.split("<TR bgColor=#ffffff>");

							if (rows[0].contains("align=center")) {
								rows[0] = rows[0].substring(0, rows[0].indexOf("</tr>"));
								rec_cols = rows[0].split("</td>");
								rec_cols[0] = rec_cols[0].substring(rec_cols[0].indexOf("<A"),
										rec_cols[0].indexOf("</a>"));
								for (int j = 0; j < rec_cols.length; j++) {
									rec_cols[j] = rec_cols[j].substring(rec_cols[j].indexOf(">") + 1);

									if (j == 0 || j == 1 || j == 4 || j == 8 || j == 9 || j == 12 || j == 13) {
										if (rec_cols[j].equals("")) {
											rec_cols[j] = "0";
										}
									}
								}

								if (rec_cols[1].contains("/")) {
									total_horse = Integer.parseInt(rec_cols[1].substring(rec_cols[1].indexOf("/") + 1));
									rec_cols[1] = rec_cols[1].substring(0, rec_cols[1].indexOf("/"));
								} else {
									total_horse = 0;
								}
								rec_cols[2] = rec_cols[2].toLowerCase();

								sql = "insert into " + table_name + " values(NULL, \"" + jockey + "\"," + race_date
										+ ",\"" + venue + "\"," + points + "," + win + "," + snd + "," + trd + ",\"";

								for (int j = 0; j < rec_cols.length; j++) {
									sql += rec_cols[j] + "\"";
									if (j == 1)
										sql += "," + total_horse;
									sql += ",\"";
								}

								sql += in_service.get(i) + "\")";

								stmt.executeUpdate(sql);
							}

							if (rows.length > 1) {
								if (has_pts == false && rows[0].contains("t")) {
									logger_.info("calculate points for jockey:" + jockey + ", date: " + race_date);
									sql = "select placing from " + table_name + " where race_date = " + race_date
											+ " and jockey = \"" + jockey + "\"";
									logger_.info(sql);
									try (ResultSet rs = stmt.executeQuery(sql)) {
										while (rs.next()) {
											if (rs.getInt(1) == 1) {
												points += 12;
												win += 1;
											} else if (rs.getInt(1) == 2) {
												points += 6;
												snd += 1;
											} else if (rs.getInt(1) == 3) {
												points += 4;
												trd += 1;
											}
										}
									}
									sql = "update " + table_name + " set points = " + points + ", win =" + win
											+ ", 2nd = " + snd + ", 3rd =" + trd + " where jockey = \"" + jockey
											+ "\" and race_date =" + race_date;
									logger_.info(sql);
									stmt.executeUpdate(sql);
								}

								String cols[] = rows[1].split("</td>");
								race_date = cols[0].substring(cols[0].indexOf("<b>") + 3, cols[0].indexOf("&nbsp;"));
								race_date = new SimpleDateFormat("yyyyMMdd")
										.format(new SimpleDateFormat("dd/MM/yyyy").parse(race_date));
								venue = cols[0].substring(cols[0].indexOf("&nbsp;") + 12, cols[0].indexOf("</b>"));
								venue = venue.substring(0, 1).toLowerCase()
										+ Character.toLowerCase(venue.charAt(venue.indexOf(" ") + 1));
								if (cols.length > 2) {
									has_pts = true;
									points = Integer.parseInt(
											cols[1].substring(cols[1].indexOf(" : ") + 3, cols[1].indexOf(" (")));
									win = Integer.parseInt(
											cols[1].substring(cols[1].indexOf("Win") + 4, cols[1].indexOf(" 2nd")));
									snd = Integer.parseInt(
											cols[1].substring(cols[1].indexOf("2nd") + 4, cols[1].indexOf(" 3rd")));
									trd = Integer.parseInt(
											cols[1].substring(cols[1].indexOf("3rd") + 4, cols[1].indexOf(" )")));
								} else {
									has_pts = false;
									points = 0;
									win = 0;
									snd = 0;
									trd = 0;
								}
							}

						}
					}
				} while (loading == true);
			}
			logger_.info("Finish updating jockey trainer info");
		} catch (SQLException se) {
			logger_.error("SQLException: ", se);
		} catch (Exception e) {
			logger_.error("Exception: ", e);
		}

	}

	public static void update_horse_form_rec(String table_name, Date last_date, int load_wait_time, Statement stmt,
			String url_addr, int first_date) {
		boolean loading = false;
		try {
			do {
				loading = false;
				logger_.info(url_addr);
				List<String> line_arr = URLConn.load_url(url_addr, load_wait_time);
				String sql = null;

				String name = "";
				String season = "";
				String last_race_date = "";
				String race_idx = "";
				Integer pla = null;
				String venue = "";
				String track = "";
				String course = "";
				Integer dist = null;
				String going = "";
				String race_class = "";
				Integer draw = null;
				Integer rtg = null;
				String trainer = "";
				String jockey = "";
				String lbw = "";
				Double odd = null;
				Integer act_wt = null;
				int total_pos = 0;
				Integer[] running_pos = new Integer[6];
				String finish_time = "";
				Integer body_wt = null;
				String gear = "";
				int cols = 0;
				boolean begin = false;
				boolean race_class_flag = false;
				boolean valid_rec = true;
				boolean retired = false;
				boolean overseas = false;
				boolean last_state = true; // true: last time the horse is in
											// service

				for (String inputLine : line_arr) {

					if (inputLine.contains("Loading.gif")) {
						logger_.info("the page is still loading");
						loading = true;
						break;
					}

					if (last_state == false)
						break;

					if (inputLine.contains("class=bigborder")) {
						begin = true;
					} else if (begin == true && inputLine.contains(" </table>")) {
						break;
					} else if (inputLine.contains("subsubheader")) {
						inputLine = inputLine.substring(inputLine.indexOf(">") + 1);
						name = inputLine.substring(0, inputLine.indexOf("  "));

						if (!inputLine.contains("title_eng_text")) {
							retired = true;
							logger_.info(name + " retired");
							String query = "select distinct in_service from `" + table_name + "` where horse = \""
									+ name + "\";";
							String in_service = "UNKOWN";
							try (ResultSet rs = stmt.executeQuery(query)) {
								if (rs.next()) {
									in_service = rs.getString(1);
								}
							}
							if (in_service.equals("Y")) {
								logger_.info("change the service state of horse " + name + " to N");
								query = "UPDATE `" + table_name + "` set in_service = \"N\" where horse = \"" + name
										+ "\";";
								stmt.executeUpdate(query);
							} else if (in_service.equals("N")) {
								last_state = false;
							}
						} else {
							name = name.substring(name.indexOf(">") + 1);
						}

						if (name.charAt(0) == ' ')
							name = name.substring(1);
						logger_.info("horse: " + name);
					}

					if (retired == true) {
						if (inputLine.contains("Season")) {
							inputLine = inputLine.substring(inputLine.indexOf("&nbsp;") + 6);
							season = inputLine.substring(0, inputLine.indexOf("<"));
						}

						if (inputLine.contains("racedate")) {

							inputLine = inputLine.substring(inputLine.indexOf("<a"));
							inputLine = inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("</"));
							race_idx = inputLine;
							/*
							 * if(inputLine.matches("[0-9]+")) race_idx =
							 * Integer.parseInt(inputLine); else race_idx = 0;
							 */
							cols = 1;
						}

						if (overseas && inputLine.contains("</tr>")) {
							overseas = false;
						}

						if (race_class_flag == true) {
							inputLine = inputLine.replaceAll("	", "");
							inputLine = inputLine.replace(" ", "");
							inputLine = inputLine.replace("&nbsp;", "");
							race_class = inputLine;
							race_class_flag = false;
						}

						if (inputLine.contains("htable_eng_rpnarrow2_text") && valid_rec) {
							String[] arr = inputLine.split("</font>");
							if (arr[0].contains("--"))
								total_pos = 0;
							else
								total_pos = arr.length - 1;
							for (int i = 0; i < arr.length - 1; i++) {
								if (arr[i].contains("--"))
									running_pos[i] = null;
								else if (i == 0)
									running_pos[i] = Integer.parseInt(arr[i].substring(arr[i].indexOf(">") + 1));
								else
									running_pos[i] = Integer.parseInt(arr[i].substring(arr[i].indexOf(";") + 7));
							}

							for (int i = arr.length - 1; i < 6; i++) {
								running_pos[i] = null;
							}

						}

						if (begin && inputLine.contains("Overseas")) {
							overseas = true;
							inputLine = inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("</"));
							inputLine = inputLine.substring(inputLine.indexOf(">") + 1);
							race_idx = inputLine;
							cols = 1;
						} else if (cols >= 1 && inputLine.contains("size") && valid_rec) {
							cols = cols + 1;
							if (cols == 2) {
								inputLine = inputLine.substring(inputLine.indexOf("size") + 5);
								inputLine = inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("<"));
								pla = check_integer(inputLine);

							} else if (cols == 3) {
								inputLine = inputLine.substring(inputLine.indexOf("size") + 5);
								last_race_date = inputLine.substring(inputLine.indexOf(">") + 1,
										inputLine.indexOf("<"));
								Date temp_date = new SimpleDateFormat("dd/MM/yyyy").parse(last_race_date);
								last_race_date = new SimpleDateFormat("yyyyMMdd")
										.format(new SimpleDateFormat("dd/MM/yyyy").parse(last_race_date));
								if (!temp_date.after(last_date)) {
									valid_rec = false;
								}

							} else if (cols == 4) {
								if (overseas) {
									inputLine = inputLine.substring(inputLine.indexOf(">") + 1,
											inputLine.lastIndexOf("</font"));
									inputLine = inputLine.substring(inputLine.indexOf(">") + 1);
									venue = inputLine.substring(0, inputLine.indexOf("<"));
									inputLine = inputLine.substring(inputLine.indexOf(">") + 1);
									inputLine = inputLine.substring(inputLine.indexOf(">") + 1);
									track = inputLine.substring(inputLine.indexOf("\"") + 1, inputLine.indexOf("\"<"));
									inputLine = inputLine.substring(inputLine.lastIndexOf(">") + 1);
									course = inputLine;
								} else {
									inputLine = inputLine.substring(inputLine.indexOf("size") + 5);
									venue = inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf(" "));

									inputLine = inputLine.substring(inputLine.indexOf("size") + 5);
									track = inputLine.substring(inputLine.indexOf(">\"") + 2,
											inputLine.indexOf("\" /"));

									inputLine = inputLine.substring(inputLine.indexOf("size") + 5);
									course = inputLine.substring(inputLine.indexOf(">\"") + 2);
									if (course.contains(" "))
										course = course.substring(0, course.indexOf(" "));
									else
										course = course.substring(0, course.indexOf("\""));
								}
							} else if (cols == 5) {
								inputLine = inputLine.substring(inputLine.indexOf("size") + 5);
								dist = check_integer(
										inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("<")));
							} else if (cols == 6) {
								inputLine = inputLine.substring(inputLine.indexOf("size") + 5);
								going = inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("<"));
								going = going.replaceAll("\\s+$", "");
							} else if (cols == 7) {
								race_class_flag = true;
							} else if (cols == 8) {
								inputLine = inputLine.substring(inputLine.indexOf("size") + 5);
								if (inputLine.contains("--"))
									draw = null;
								else
									draw = Integer.parseInt(
											inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("<")));
							} else if (cols == 9) {
								inputLine = inputLine.substring(inputLine.indexOf("size") + 5);
								if (inputLine.contains("--"))
									rtg = null;
								else
									rtg = Integer.parseInt(
											inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("<")));
							} else if (cols == 10) {
								inputLine = inputLine.substring(inputLine.indexOf("size") + 5);
								trainer = inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("<"));
								trainer = trainer.replaceAll("\\s+$", "");
							} else if (cols == 11) {
								inputLine = inputLine.substring(inputLine.indexOf("size") + 5);
								jockey = inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("<"));
								jockey = jockey.replaceAll("\\s+$", "");
							} else if (cols == 12) {
								inputLine = inputLine.substring(inputLine.indexOf("size") + 5);
								lbw = inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("<"));
								lbw = lbw.replaceAll("\\s+$", "");
							} else if (cols == 13) {
								inputLine = inputLine.substring(inputLine.indexOf("size") + 5);
								if (inputLine.contains("--"))
									odd = null;
								else
									odd = Double.parseDouble(
											inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("<")));
							} else if (cols == 14) {
								inputLine = inputLine.substring(inputLine.indexOf("size") + 5);
								if (inputLine.contains("--"))
									act_wt = null;
								else
									act_wt = Integer.parseInt(
											inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("<")));
							} else if (cols == 15) {
								inputLine = inputLine.substring(inputLine.indexOf("size") + 5);
								finish_time = inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("<"));
							} else if (cols == 16) {
								inputLine = inputLine.substring(inputLine.indexOf("size") + 5);
								if (inputLine.contains("--"))
									body_wt = null;
								else
									body_wt = Integer.parseInt(
											inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("<")));
							} else if (cols == 17) {
								inputLine = inputLine.substring(inputLine.indexOf("size") + 5);
								gear = inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("<"));

								if (sql == null)
									sql = "insert into `" + table_name + "` values ";
								sql += "(NULL, \"" + name + "\", \"";
								if (first_date > 0)
									sql += first_date + "\",\"";

								sql += season + "\", \"" + race_idx + "\", " + pla + ", \"" + last_race_date + "\", \""
										+ venue + "\", \"" + track + "\", \"" + course + "\", " + dist + ", \"" + going
										+ "\", \"" + race_class + "\", " + draw + ", " + rtg + ", \"" + trainer
										+ "\", \"" + jockey + "\", \"" + lbw + "\", " + odd + ", " + act_wt + ", "
										+ total_pos + ", " + running_pos[0] + ", " + running_pos[1] + ", "
										+ running_pos[2] + ", " + running_pos[3] + ", " + running_pos[4] + ", "
										+ running_pos[5] + ", \"" + finish_time + "\", " + body_wt + ", \"" + gear
										+ "\", \"N\")";

								if (sql.length() > 2000) {
									sql += ";";
									stmt.executeUpdate(sql);
									sql = null;
								} else {
									sql += ",";
								}

							}
						}
					} else {
						if (inputLine.contains("htable_eng_bold_text")) {
							inputLine = inputLine.substring(inputLine.indexOf(";") + 1);
							season = inputLine.substring(0, inputLine.indexOf(" "));
						}

						if (race_class_flag == true) {
							race_class = inputLine;
							race_class = race_class.replace("	", "");
							race_class = race_class.replaceAll("\\s+$", "");
							race_class_flag = false;
						}

						if (cols == 11 && !inputLine.contains("Trainer") && valid_rec) {
							trainer = inputLine;
							trainer = trainer.replaceAll("\\s+$", "");
							trainer = trainer.replaceAll("	", "");
							cols++;
						} else if (inputLine.contains("htable_eng_text") && inputLine.contains("racedate")) {
							race_idx = inputLine.substring(inputLine.indexOf("t>") + 2, inputLine.indexOf("</"));
							cols = 1;
						} else if (begin && inputLine.contains("Overseas")) {
							overseas = true;
							race_idx = inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("</"));
							cols = 1;
						} else if (overseas && inputLine.contains("</tr>")) {
							overseas = false;
						} else if (cols >= 1 && inputLine.contains("htable_eng_") && valid_rec) {
							++cols;
							if (cols == 14 && !inputLine.contains("Jockey")) {
								jockey = null;
								++cols;
							}
							if (cols == 2) {
								inputLine = inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("</"));
								if (inputLine.contains(">"))
									inputLine = inputLine.substring(inputLine.indexOf(">") + 1);
								pla = check_integer(inputLine);
								/*
								 * if(inputLine.matches("[0-9]+")) pla =
								 * Integer.parseInt(inputLine); else pla = 0;
								 */
							} else if (cols == 3) {
								last_race_date = inputLine.substring(inputLine.indexOf(">") + 1,
										inputLine.indexOf("</"));
								Date temp_date = new SimpleDateFormat("dd/MM/yy").parse(last_race_date);
								last_race_date = new SimpleDateFormat("yyyyMMdd")
										.format(new SimpleDateFormat("dd/MM/yy").parse(last_race_date));

								if (!temp_date.after(last_date)) {
									valid_rec = false;
								}
							} else if (cols == 4) {
								if (overseas) {
									inputLine = inputLine.substring(inputLine.indexOf(">") + 1,
											inputLine.indexOf("</td"));
									venue = inputLine.substring(0, inputLine.indexOf("<"));
									inputLine = inputLine.substring(inputLine.indexOf(">") + 1);
									track = inputLine.substring(inputLine.indexOf("\"") + 1, inputLine.indexOf("\"<"));
									inputLine = inputLine.substring(inputLine.indexOf("</font>") + 7);
									course = inputLine;
								} else {
									inputLine = inputLine.substring(inputLine.indexOf(">") + 1);
									venue = inputLine.substring(0, inputLine.indexOf(" "));
									inputLine = inputLine.substring(inputLine.indexOf(">") + 2);
									track = inputLine.substring(0, inputLine.indexOf("\""));
									inputLine = inputLine.substring(inputLine.indexOf(">") + 2);
									course = inputLine.substring(0, inputLine.indexOf("\""));
									course = course.replaceAll(" ", "");
								}
							} else if (cols == 5) {
								dist = check_integer(
										inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("</")));
							} else if (cols == 6) {
								going = inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("</"));
								going = going.replaceAll("\\s+$", "");
							} else if (cols == 7) {
								race_class_flag = true;
							} else if (cols == 9) {
								if (inputLine.contains("--"))
									draw = null;
								else
									draw = Integer.parseInt(
											inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("</")));
							} else if (cols == 10) {
								if (inputLine.contains("--"))
									rtg = null;
								else
									rtg = Integer.parseInt(
											inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("</")));
							} else if (cols == 12) {
								trainer = inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("</"));
								trainer = trainer.replaceAll("\\s+$", "");
							} else if (cols == 14) {
								jockey = inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("</"));
								jockey = jockey.replaceAll("\\s+$", "");
							} else if (cols == 15) {
								if (inputLine.contains("text2"))
									lbw = inputLine.substring(inputLine.indexOf("t2>") + 3, inputLine.indexOf("</"));
								else if (!inputLine.contains("text>"))
									lbw = inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("</"));
								else
									lbw = inputLine.substring(inputLine.indexOf("t>") + 2, inputLine.indexOf("</"));
								lbw = lbw.replaceAll("\\s+$", "");
							} else if (cols == 16) {
								if (inputLine.contains("--"))
									odd = null;
								else
									odd = Double.parseDouble(
											inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("</")));
							} else if (cols == 17) {
								act_wt = Integer.parseInt(
										inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("</")));
							} else if (cols == 19) {
								String[] arr = inputLine.split("</font>");
								if (arr[0].contains("--"))
									total_pos = 0;
								else
									total_pos = arr.length - 1;
								for (int i = 0; i < arr.length - 1; i++) {
									if (arr[i].contains("--"))
										running_pos[i] = null;
									else if (i == 0)
										running_pos[i] = Integer.parseInt(arr[i].substring(arr[i].indexOf(">") + 1));
									else
										running_pos[i] = Integer.parseInt(arr[i].substring(arr[i].indexOf(";") + 7));
								}

								for (int i = arr.length - 1; i < 6; i++) {
									running_pos[i] = null;
								}
							} else if (cols == 20) {
								finish_time = inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("</"));
							} else if (cols == 21) {
								if (inputLine.contains("--"))
									body_wt = null;
								else
									body_wt = Integer.parseInt(
											inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("</")));
							} else if (cols == 22) {
								gear = inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("</"));

								if (sql == null)
									sql = "insert into `" + table_name + "` values ";

								sql += "(NULL, \"" + name + "\", \"";
								if (first_date > 0)
									sql += first_date + "\",\"";

								sql += season + "\", \"" + race_idx + "\", " + pla + ", \"" + last_race_date + "\", \""
										+ venue + "\", \"" + track + "\", \"" + course + "\", " + dist + ", \"" + going
										+ "\", \"" + race_class + "\", " + draw + ", " + rtg + ", \"" + trainer
										+ "\", \"" + jockey + "\", \"" + lbw + "\", " + odd + ", " + act_wt + ", "
										+ total_pos + ", " + running_pos[0] + ", " + running_pos[1] + ", "
										+ running_pos[2] + ", " + running_pos[3] + ", " + running_pos[4] + ", "
										+ running_pos[5] + ", \"" + finish_time + "\", " + body_wt + ", \"" + gear
										+ "\", \"Y\")";

								if (sql.length() > 2000) {
									sql += ";";
									stmt.executeUpdate(sql);
									sql = null;
								} else {
									sql += ",";
								}
							}
						}

					}
				}

				if (sql != null) {
					sql = sql.substring(0, sql.length() - 1) + ";";
					stmt.executeUpdate(sql);
				}

			} while (loading == true);
		} catch (Exception e) {
			logger_.error("Exception:", e);
		}
	}

	public static void update_all_horse_form_rec(String race_date, String pre_date, int load_wait_time, Statement stmt,
			Connection con, List<String> horse_code_list) {
		logger_.info("Start getting the horse form records for date " + race_date);
		try {
			String table_name = "horse_form_rec_" + race_date;
			String pre_table = "horse_form_rec_" + pre_date;
			String sql = null;
			Date last_date = new Date();

			DatabaseMetaData dbm = con.getMetaData();
			try (ResultSet tables = dbm.getTables(null, null, pre_table, null)) {
				if (tables.next()) {
					sql = "CREATE TABLE if not exists " + table_name + " like " + pre_table;
					stmt.executeUpdate(sql);
					sql = "insert into " + table_name + " select * from " + pre_table;
					stmt.executeUpdate(sql);
					sql = "select race_date from " + pre_table + " order by race_date desc limit 1";
					try (ResultSet rs = stmt.executeQuery(sql)) {
						if (rs.next()) {
							last_date = new SimpleDateFormat("yyyyMMdd").parse(rs.getString(1));
						}
					}
				} else {

					sql = "CREATE TABLE if not exists " + table_name
							+ " (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, horse varchar(30), season varchar(20), race_idx varchar(20), pla INT, race_date varchar(10), venue varchar(50), track varchar(20), course varchar(20),"
							+ "dist INT, going varchar(20), race_class varchar(30), draw INT, rtg INT, trainer varchar(30), jockey varchar(30), lbw varchar(30), odd DOUBLE PRECISION(10,2), act_wt INT, total_pos INT, running_pos1 INT,"
							+ "running_pos2 INT, running_pos3 INT, running_pos4 INT, running_pos5 INT, running_pos6 INT, finish_time varchar(20), body_wgt INT, gear varchar(30), in_service varchar(1)) engine=MYISAM";
					stmt.execute(sql);
					last_date = new SimpleDateFormat("yyyyMMdd").parse("00000000");
				}
			}

			for (int horse = 0; horse < horse_code_list.size();) {
				// logger_.info("horse: " + horse_code_list.get(horse));
				String url_addr = "http://www.hkjc.com/english/racing/horse.asp?HorseNo=" + horse_code_list.get(horse)
						+ "&Option=1#htop";

				update_horse_form_rec(table_name, last_date, load_wait_time, stmt, url_addr, 0);
				horse++;
			}

			logger_.info("Finish getting the horse form records for date " + race_date);
		} catch (Exception e) {
			logger_.error("Exception:", e);
		}
	}

	public static void update_new_horse_info(String date, String pre_date, int load_wait_time, Statement stmt,
			Connection con) {
		logger_.info("Start updating new horse info for date " + date);
		try {
			String table_name = "new_horse_" + date;
			String pre_table = "new_horse_" + pre_date;
			String sql = null;
			Date last_date = new Date();

			DatabaseMetaData dbm = con.getMetaData();
			try (ResultSet tables = dbm.getTables(null, null, pre_table, null)) {
				if (tables.next()) {
					sql = "CREATE TABLE if not exists " + table_name + " like " + pre_table;
					stmt.executeUpdate(sql);
					sql = "insert into " + table_name + " select * from " + pre_table;
					stmt.executeUpdate(sql);
					sql = "select race_date from " + pre_table + " order by race_date desc limit 1";
					try (ResultSet rs = stmt.executeQuery(sql)) {
						if (rs.next()) {
							last_date = new SimpleDateFormat("yyyyMMdd").parse(rs.getString(1));
						}
					}
				} else {

					sql = "CREATE TABLE if not exists " + table_name
							+ " (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, horse varchar(30), first_race_date varchar(10), season varchar(20), race_index varchar(20), placing INT, race_date varchar(10), venue varchar(50), "
							+ "track varchar(20), course varchar(20), dist INT, going varchar(20), race_class varchar(30), draw INT, rtg INT, trainer varchar(30), jockey varchar(30), LBW varchar(30), win_odd DOUBLE PRECISION(10,2), "
							+ "act_wt INT, total_running_pos INT, running_pos1 INT, running_pos2 INT, running_pos3 INT, running_pos4 INT, running_pos5 INT, running_pos6 INT, finish_time varchar(20), body_wgt INT, "
							+ "gear varchar(30), in_service varchar(1)) engine=MYISAM";
					stmt.execute(sql);
					last_date = new SimpleDateFormat("yyyyMMdd").parse("00000000");
				}
			}

			boolean loading = false;

			// get race date list
			logger_.info("get race date list");
			List<Integer> race_date_list = new ArrayList<Integer>();
			String url_base = "http://www.hkjc.com/english/racinginfo/newhorse.asp";

			do {
				loading = false;
				List<String> line_arr = URLConn.load_url(url_base, load_wait_time);
				for (String inputLine : line_arr) {

					if (inputLine.contains("Loading.gif")) {
						logger_.info("the page is still loading");
						loading = true;
						break;
					}

					if (inputLine.contains("option value") && !inputLine.contains("\"")) {
						String[] date_arr = inputLine.split("</option>");

						for (int i = 0; i < date_arr.length; i++) {
							int date_opt = Integer.parseInt(
									date_arr[i].substring(date_arr[i].indexOf("=") + 1, date_arr[i].indexOf("=") + 9));
							if (date_opt < Integer.parseInt(date)) {
								race_date_list.add(date_opt);
							}
						}

						break;
					}

				}
			} while (loading);

			// get horse list
			int total_days = race_date_list.size();
			List<ArrayList<Integer>> race_list = new ArrayList<ArrayList<Integer>>();
			List<ArrayList<String>> horse_list = new ArrayList<ArrayList<String>>();
			logger_.info("get new horse list");

			for (int day = 0; day < total_days; day++) {
				ArrayList<Integer> race = new ArrayList<Integer>();
				ArrayList<String> horse = new ArrayList<String>();

				do {
					loading = false;

					List<String> line_arr = URLConn.load_url(url_base + "?racedate=" + race_date_list.get(day),
							load_wait_time);
					for (String inputLine : line_arr) {

						if (inputLine.contains("Loading.gif")) {
							logger_.info("the page is still loading");
							loading = true;
							break;
						}

						if (inputLine.contains("option value") && inputLine.contains(",")) {
							String[] arr = inputLine.split("</option>");

							for (int i = 0; i < arr.length; i++) {
								race.add(Integer
										.parseInt(arr[i].substring(arr[i].indexOf("=") + 1, arr[i].indexOf(","))));
								arr[i] = arr[i].substring(arr[i].indexOf(",") + 1, arr[i].indexOf(">"));
								horse.add(arr[i].substring(0, arr[i].indexOf(" ")));
							}

						}
					}

				} while (loading);
				race_list.add(race);
				horse_list.add(horse);
			}

			// get detailed info
			for (int day = 0; day < total_days; day++) {
				logger_.info("Start fetching new horse for date " + race_date_list.get(day));
				for (int horse = 0; horse < horse_list.get(day).size(); horse++) {
					// logger_.info("horse: " + horse_list.get(day).get(horse));
					// url = new
					// URL("http://www.hkjc.com/english/racing/horse.asp?HorseNo=P395&Option=1#htop");

					String url_addr = "http://www.hkjc.com/english/racing/horse.asp?HorseNo="
							+ horse_list.get(day).get(horse) + "&Option=1#htop";
					update_horse_form_rec(table_name, last_date, load_wait_time, stmt, url_addr,
							race_date_list.get(day));
				}
			}

		} catch (Exception e) {
			logger_.error("Exception: ", e);
		}
	}

	public static List<String> get_all_horse_code(int load_wait_time) {

		logger_.info("Start fetching all horse codes");

		List<String> horse_code_list = new ArrayList<String>();
		List<String> char_list = new ArrayList<String>();
		String url_base = "http://www.hkjc.com/english/racing/selecthorsebychar.asp";

		boolean loading = false;

		try {
			do {
				loading = false;

				List<String> line_arr = URLConn.load_url(url_base, load_wait_time);
				for (String inputLine : line_arr) {
					if (inputLine.contains("Loading.gif")) {
						logger_.info("the page is still loading");
						loading = true;
						break;
					}

					if (inputLine.contains("ordertype")) {
						String[] arr = inputLine.split("</font>");
						for (int i = 1; i < arr.length - 1; i++) {
							arr[i] = arr[i].substring(arr[i].lastIndexOf("t>") + 2, arr[i].indexOf("</"));
							char_list.add(arr[i]);
						}
					}

					if (inputLine.contains("<td class=table_eng_text") && inputLine.contains("<a")) {
						inputLine = inputLine.substring(inputLine.indexOf("<a"), inputLine.indexOf("</a>"));
						inputLine = inputLine.substring(inputLine.indexOf("t>") + 2);
						horse_code_list.add(inputLine);
					}

				}

			} while (loading == true);

			for (int i = 0; i < char_list.size(); i++) {
				String url_addr = url_base + "?ordertype=" + char_list.get(i);
				do {
					loading = false;
					List<String> line_arr = URLConn.load_url(url_addr, load_wait_time);
					for (String inputLine : line_arr) {

						if (inputLine.contains("Loading.gif")) {
							logger_.info("the page is still loading");
							loading = true;
							break;
						}

						if (inputLine.contains("<td class=table_eng_text") && inputLine.contains("<a")) {
							inputLine = inputLine.substring(inputLine.indexOf("No=") + 3);
							inputLine = inputLine.substring(0, inputLine.indexOf("\""));
							horse_code_list.add(inputLine);
						}

					}

				} while (loading == true);
			}

		} catch (Exception e) {
			logger_.error("Exception: ", e);
		}

		logger_.info("Finish fetching all horse codes:" + horse_code_list.size());
		return horse_code_list;
	}

	public static void update_horse_performance(String date, String pre_date, int load_wait_time, Statement stmt,
			List<String> horse_code_list) {
		logger_.info("Start updating the horse performance");

		String url_base = "http://racing.hkjc.com/racing/info/Horse/Performance/English/";
		boolean loading = false;

		try {

			String table_name = "horse_performance_" + date;

			String sql = "CREATE TABLE if not exists " + table_name
					+ " (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, horse varchar(20), type varchar(15),"
					+ " venue varchar(5), track varchar(10), criterion varchar(20), run INT, win INT, "
					+ "2nd INT, 3rd INT, unplaced INT)";
			stmt.executeUpdate(sql);

			sql = null;

			for (int horse = 0; horse < horse_code_list.size(); horse++) {
				logger_.info("get the performance of horse: " + horse_code_list.get(horse));
				String url_addr = url_base + horse_code_list.get(horse);
				String name = " ";
				String type = " ";
				String venue = " ";
				String track = " ";
				do {
					loading = false;

					List<String> line_arr = URLConn.load_url(url_addr, load_wait_time);
					for (String inputLine : line_arr) {

						if (inputLine.contains("Loading.gif")) {
							logger_.info("the page is still loading");
							loading = true;
							break;
						}

						if (inputLine.contains("Performance:")) {

							inputLine = inputLine.substring(inputLine.indexOf("ce:") + 4);
							name = inputLine.substring(0, inputLine.indexOf("<"));
							if (!inputLine.contains("No Previous Race Record Found.")) {
								inputLine = inputLine.substring(inputLine.indexOf("<table"),
										inputLine.lastIndexOf("</table>"));
								String[] arr = inputLine.split("</tr>");
								for (int i = 1; i < arr.length; i++) {
									if (arr[i].contains("blue")) {
										type = arr[i].substring(arr[i].lastIndexOf("\">") + 2, arr[i].indexOf("</"));
										venue = " ";
										track = " ";
									} else {
										String[] cols = arr[i].split("</td>");
										for (int j = 0; j < cols.length; j++) {
											cols[j] = cols[j].substring(cols[j].lastIndexOf(">") + 1);
											if (j == 0) {
												if (cols[j].contains(" ")) {
													venue = cols[j].substring(0, 1)
															+ cols[j].charAt(cols[j].indexOf(" ") + 1);
													track = cols[j].substring(cols[j].lastIndexOf(" ") + 1);
												} else if (!cols[j].contains("&nbsp;")) {
													track = cols[0];
												}
											}

											if (j == 1 && cols[j].contains("Total")) {
												cols[j] = "Total";
											}

											if (j == 1 && cols[j].matches("^[0-9]+m$")) {
												cols[j] = cols[j].substring(0, cols[j].length() - 1);
											}

										}

										if (sql == null)
											sql = "insert into `" + table_name + "` values ";

										sql += "(NULL, \"" + name + "\", \"" + type + "\", \"" + venue + "\", \""
												+ track + "\", \"" + cols[1] + "\", " + Integer.parseInt(cols[2]) + ", "
												+ Integer.parseInt(cols[3]) + ", " + Integer.parseInt(cols[4]) + ", "
												+ Integer.parseInt(cols[5]) + ", " + Integer.parseInt(cols[6]) + ")";

										if (sql.length() > 2000) {
											sql += ";";
											stmt.executeUpdate(sql);
											sql = null;
										} else {
											sql += ",";
										}

									}
								}
							}

						}

					}

				} while (loading == true);

			}

			if (sql != null) {
				sql = sql.substring(0, sql.length() - 1) + ";";
				stmt.executeUpdate(sql);
			}

		} catch (Exception e) {
			logger_.error("Exception: ", e);
		}

		logger_.info("Finish updating the horse performance");
	}

	public static void update_same_sire_performance(String date, String pre_date, int load_wait_time, Statement stmt,
			List<String> horse_code_list) {
		logger_.info("Start updating the same sire performance");
		try {
			String table_name = "same_sire_performance_" + date;

			String sql = "CREATE TABLE if not exists " + table_name
					+ " (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, horse varchar(20), sire varchar(20),"
					+ " same_sire varchar(20), in_service varchar(5), win INT, 2nd INT, 3rd INT, run INT, "
					+ "  distance_won varchar(20))";
			stmt.executeUpdate(sql);

			boolean loading = false;

			List<String> encoded_code_list = new ArrayList<String>();
			boolean occurred = false;
			List<String> horse_name_list = new ArrayList<String>();

			for (int i = 0; i < horse_code_list.size(); i++) {
				String url_addr = "http://www.hkjc.com/english/racing/horse.asp?HorseNo=" + horse_code_list.get(i);
				logger_.info(url_addr);

				do {
					loading = false;
					occurred = false;

					List<String> line_arr = URLConn.load_url(url_addr, load_wait_time);
					for (String inputLine : line_arr) {
						if (inputLine.contains("Loading.gif")) {
							logger_.info("the page is still loading");
							Thread.sleep(5 * 1000);
							loading = true;
							break;
						}

						if (inputLine.contains("subsubheader")) {
							inputLine = inputLine.substring(inputLine.indexOf(">") + 1);
							String name = inputLine.substring(0, inputLine.indexOf("  "));
							if (inputLine.contains("title_eng_text")) {
								name = name.substring(name.indexOf(">") + 1);
							}
							if (name.charAt(0) == ' ')
								name = name.substring(1);
							horse_name_list.add(name);
						}

						if (inputLine.contains("SameSire")) {
							occurred = true;
							inputLine = inputLine.substring(inputLine.indexOf("english") + 8,
									inputLine.indexOf(",") - 1);
							encoded_code_list.add(inputLine);
							break;
						}
					}

				} while (loading == true);

				if (occurred == false) {
					horse_name_list.remove(horse_name_list.size() - 1);
				}
			}

			String sire = null;

			sql = null;

			// encoded_code_list.size()
			for (int horse = 0; horse < horse_name_list.size(); horse++) {
				String name = horse_name_list.get(horse);
				logger_.info("get the same sire performance for horse: " + name);
				String url_addr = "http://racing.hkjc.com/racing/info/Horse/SameSire/english/"
						+ encoded_code_list.get(horse) + "/ALL";
				logger_.info(url_addr);

				do {
					loading = false;

					List<String> line_arr = URLConn.load_url(url_addr, load_wait_time);
					for (String inputLine : line_arr) {
						if (inputLine.contains("Loading.gif")) {
							logger_.info("the page is still loading");
							Thread.sleep(5 * 1000);
							loading = true;
							break;
						}

						if (inputLine.contains("Same Sire Performance -")) {
							inputLine = inputLine.substring(inputLine.indexOf("-") + 2);
							sire = inputLine.substring(0, inputLine.indexOf("</"));
						}

						if (inputLine.contains("Distance")) {
							inputLine = inputLine.substring(inputLine.indexOf("<table"),
									inputLine.lastIndexOf("</table"));
							String[] arr = inputLine.split("</tr>");
							String in_service = null;
							for (int i = 1; i < arr.length; i++) {
								String[] cols = arr[i].split("</td>");
								for (int j = 0; j < cols.length; j++) {
									cols[j] = cols[j].substring(cols[j].indexOf(">") + 1);
									if (j == 0) {
										if (!cols[j].contains("</a>"))
											cols[j] = cols[j].substring(cols[j].indexOf(">") + 1);
										else
											cols[j] = cols[j].substring(cols[j].indexOf(")\">") + 3,
													cols[j].indexOf("</"));
										if (cols[j].contains("Retired")) {
											in_service = "N";
											cols[j] = cols[j].substring(0, cols[j].indexOf(" ("));
										} else if (cols[j].contains("Total")) {
											in_service = "";
										} else {
											in_service = "Y";
										}
									} else {
										if (cols[j].contains("(")) {
											cols[j] = cols[j].substring(0, cols[j].indexOf("("));
										}

										cols[j] = cols[j].replace("&nbsp;", "");

									}
								}

								if (sql == null)
									sql = "insert into `" + table_name + "` values ";

								sql += "(NULL, \"" + name + "\", \"" + sire + "\", \"" + cols[0] + "\", \"" + in_service
										+ "\", " + Integer.parseInt(cols[1]) + ", " + Integer.parseInt(cols[2]) + ", "
										+ Integer.parseInt(cols[3]) + ", " + Integer.parseInt(cols[4]) + ", \""
										+ cols[5] + "\")";

								if (sql.length() > 2000) {
									sql += ";";
									stmt.executeUpdate(sql);
									sql = null;
								} else {
									sql += ",";
								}

							}
						}

					}

				} while (loading == true);
			}

			if (sql != null) {
				sql = sql.substring(0, sql.length() - 1) + ";";
				stmt.executeUpdate(sql);
			}

		} catch (Exception e) {
			logger_.error("Exception: ", e);
		}

		logger_.info("Finish updating the same sire performance");
	}
}
